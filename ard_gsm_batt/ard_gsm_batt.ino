/**
 Project info:

 A2: AC in
 A3: BATT in

 For 24V battery:
	R1: 95K3
	R2: 18K0
	THRESBATT=24.6
	COEFBATT=1.0

 For 12V battery:
	R1: 27K1
	R2: 14K8  
	THRESBATT=12.3
	COEFBATT=1.0

 For AC:
	R1: 1K74
	R2: 4K2
	THRESAC=2.5
	COEFAC=1.0
*/



#define MODULE_2
#ifdef MODULE_1
#define PIN_RELAY_1	5	
#define PIN_RELAY_2	6
#define PIN_RELAY_3	7
#define PIN_RELAY_4	8
#define PIN_GSM_TX	2
#define PIN_GSM_RX	3
#else
#define PIN_RELAY_1	3	   
#define PIN_RELAY_2	2
#define PIN_RELAY_3	5
#define PIN_RELAY_4	4
#define PIN_GSM_TX	7
#define PIN_GSM_RX	8
#endif

#include "SIM900.h"
#include <SoftwareSerial.h>
#include <EEPROM.h>
#include "sms.h"
#include "app_strings.h"
#include "Conf.h"

#define BATT_24V	1

#define DBG Serial
#define DEF_BATT_FAULT_VALUE 12.3
#define EEPROM_CONF_HEADER	0xABCD
#define EEPROM_CONF_VERSION	1
#define EEPROM_CONF_POS		0
#define EEPROM_ADC_HEADER	0xDADA
#define EEPROM_ADC_POS		0x2FF	// position of adc data in eeprom
#define EEPROM_ADC_VERSION	1
#define CODE_VERSION	1
#define ADC_THRESSHOLD_AC	1.5
#define ADC_THRESSHOLD_BATT	12.4
#define TMR_SCAN_FOR_SMS	3000
#define TMR_CSQ_TEST		60000
#define CNTR_BATT_AC_FAULT	30
#define DEFAULT_PASSWD		"123456"

#define SIZE_TEL_NUM	15
#define SIZE_PASSW		10
#define SIZE_IMEI		16
#define SIZE_SMS		160

#define R1_ac	174		//1K74
#define R2_ac	412		//4K12

#ifdef BATT_24V
	// definitions for 24V batt
	#define R1_batt	953		//95K3
	#define R2_batt	180		//18K
#else
	//definitions for 12V batt	
	#define R1_batt	271		//27K1
	#define R2_batt	148		//14K8
#endif


#define PIN_GSM_PWR	9
#define PIN_LED		13

#define PIN_ADC_AC		A2
#define PIN_ADC_BATT	A3

#define GSM_CMD		0
#define UART_CMD	1



#define NUM_OF_RELAYS	4
typedef enum {
	RELAY_INDEX_1,
	RELAY_INDEX_2,
	RELAY_INDEX_3,
	RELAY_INDEX_4
} en_relays_index;
uint8_t glb_relay_pin[NUM_OF_RELAYS] = { PIN_RELAY_1, PIN_RELAY_2, PIN_RELAY_3, PIN_RELAY_4 };

typedef enum {
	SEND_SMS_AC_FAULT = (1 << 0),
	SEND_SMS_AC_RESTORE = (1 << 1),
	SEND_SMS_BATT_FAULT = (1 << 2),
	SEND_SMS_BATT_RESTORE = (1 << 3),
	SEND_SMS_PASSWD_RESET = (1 << 4),
	SEND_SMS_TEL_CHANGE = (1 << 5),
	SEND_SMS_SYS_STATUS = (1 << 6),
	SEND_SMS_RESP = (1 << 7),
	SEND_SMS_RELAY_STATUS = (1 << 8)
} en_todo_jobs;


typedef struct {
	uint8_t enable_tel1 : 1;
	uint8_t enable_tel2 : 1;
	uint8_t enable_tel_passwd : 1;
	uint8_t nu_1 : 1;

	uint8_t on_ac_fail : 1;
	uint8_t on_ac_restore : 1;
	uint8_t on_batt_fault : 1;
	uint8_t on_batt_restore : 1;

	uint8_t on_passw_reset : 1;
	uint8_t on_tel_change : 1;
	uint8_t ready_to_send : 1;
	uint8_t sent : 1;
} tp_conf_sms_flags;


#pragma pack(1)
typedef struct {
	uint16_t	todo;	// todo jobs (see en_todo_jobs)
	uint16_t	curr;
	uint16_t	done;
	uint8_t		sms_ready : 1;
	uint8_t		sms_sent : 1;
} tp_sys;


#pragma pack(1)
typedef struct {
	double		cal_coef_batt;
	double		cal_coef_ac;
	double		thresshold_batt;
	double		thresshold_ac;
} tp_conf_adc;


typedef enum {
	RELAY_STATUS_OFF,
	RELAY_STATUS_ON
} en_relay_status;


typedef struct {
	uint8_t init_value;
	uint32_t pulse_delay;
} tp_relay_conf;


#pragma pack(1)
typedef struct {
	char		tel1[SIZE_TEL_NUM];	// User 1 telephone number
	char		tel2[SIZE_TEL_NUM];	// User 2 telephone number
	char		passwd[SIZE_PASSW];	// Password for sms
	char		imei[SIZE_IMEI + 1];
	tp_relay_conf	relay[NUM_OF_RELAYS];
	float		batt_fault_value;
	tp_conf_sms_flags flags;
} tp_conf;


typedef enum {
	GSM_STATUS_UNIT,
	GSM_STATUS_OK,
	GSM_STATUS_NO_SIGNAL
} en_gsm_status;


typedef struct {
	uint8_t status;
	uint8_t rssi;
	uint8_t ber;
} tp_gsm_status;


typedef struct {
	uint16_t	value;
	uint8_t		samples;
	uint8_t		skip_samples;
	double		volts;
} tp_adc_channel;


typedef struct {
	tp_adc_channel batt;
	tp_adc_channel ac;
} tp_adc_values;


typedef struct {
	uint8_t		curr_status;
	uint8_t		wanted_status;
	uint32_t	relay_tmr;
} tp_relay_status;


typedef struct {
	tp_conf			conf;
	tp_conf_adc		ee_adc;
	tp_sys			status;
	tp_gsm_status	gsm;
	char			sms_text[SIZE_SMS];
	char			recv_num[SIZE_TEL_NUM];
	char			recv_text[SIZE_SMS];
	tp_adc_values	adc;
	uint32_t		prev_sms_scan;
	uint32_t		prev_csq_test;
	uint32_t		tmr_batt_test;
	uint32_t		tmr_ac_test;
	uint8_t			cntr_batt;
	uint8_t			cntr_ac;
	tp_relay_status relay[NUM_OF_RELAYS];
} tp_glb;

tp_glb glb;

//If you want to use the Arduino functions to manage SMS, uncomment the lines below.
SMSGSM sms;
Conf	*cfg_adc;
Conf	*cfg_conf;

const char const_device_text[] = "[GSM power monitor v.1.0]";	// version

void setup()
{
	int i = 0;

	//powerUpOrDown();	// power on SIM900

	pinMode(PIN_LED, OUTPUT);
	pinMode(PIN_RELAY_1, OUTPUT);
	pinMode(PIN_RELAY_2, OUTPUT);
	pinMode(PIN_RELAY_3, OUTPUT);
	pinMode(PIN_RELAY_4, OUTPUT);

	/* add setup code here */
	DBG.begin(9600);
	DBG.println(const_device_text);
	DBG.println(F("contact: dimtass@gmail.com"));

	// Load EEPROM
	cfg_conf = new Conf(EEPROM_CONF_HEADER, EEPROM_CONF_VERSION, EEPROM_CONF_POS, (uint8_t*)&glb.conf, sizeof(tp_conf));

	// Pass default values
	tp_conf tmp_conf;
	memset(tmp_conf.tel1, 0, SIZE_TEL_NUM);
	memset(tmp_conf.tel2, 0, SIZE_TEL_NUM);
	memset(tmp_conf.passwd, 0, SIZE_PASSW);
	strcpy_P(tmp_conf.passwd, FSTR(strInd_036));	//123456 // default passwd
	memset(tmp_conf.imei, 0, SIZE_IMEI);
	for (i = 0; i < NUM_OF_RELAYS; i++) {
		tmp_conf.relay[i].init_value = RELAY_STATUS_OFF;
		tmp_conf.relay[i].pulse_delay = 0;
	}
	tmp_conf.batt_fault_value = DEF_BATT_FAULT_VALUE;
	tmp_conf.flags.enable_tel1 = 1;
	tmp_conf.flags.enable_tel2 = 1;
	tmp_conf.flags.enable_tel_passwd = 1;
	tmp_conf.flags.on_ac_fail = 1;
	tmp_conf.flags.on_ac_restore = 1;
	tmp_conf.flags.on_batt_fault = 1;
	tmp_conf.flags.on_batt_restore = 1;
	tmp_conf.flags.on_passw_reset = 1;
	tmp_conf.flags.on_tel_change = 1;
	cfg_conf->SetDefaultValues((uint8_t*)&tmp_conf, sizeof(tp_conf));
	// Load configuration
	cfg_conf->Load();

	DBG.print("TEL1: "); DBG.println(glb.conf.tel1);


	cfg_adc = new Conf(EEPROM_ADC_HEADER, EEPROM_ADC_VERSION, EEPROM_ADC_POS, (uint8_t*)&glb.ee_adc, sizeof(tp_conf_adc));
	tp_conf_adc tmp_adc_conf;
	tmp_adc_conf.cal_coef_ac = 1.0;
	tmp_adc_conf.thresshold_ac = ADC_THRESSHOLD_AC;
	tmp_adc_conf.cal_coef_batt = 1.0;
	tmp_adc_conf.thresshold_batt = ADC_THRESSHOLD_BATT;

	cfg_adc->SetDefaultValues((uint8_t*)&tmp_adc_conf, sizeof(tp_conf_adc));
	cfg_adc->Load();
					   

	glb.gsm.status = GSM_STATUS_UNIT;

_retry:
	// Connect to GSM modem
	if (gsm.begin(9600)) {
		DBG.println(F("[GSM]: ready"));
		glb.gsm.status = GSM_STATUS_OK;
	}
	else {
		DBG.println(F("[GSM]: failed to init!"));
		powerUpOrDown();
		goto _retry;
	}

	// Check device IMEI
	CheckIMEI();

	// reset values
	glb.status.curr = 0;
	glb.status.done = 0;
	glb.status.todo = 0;
	memset(glb.recv_num, 0, SIZE_TEL_NUM);
	memset(glb.recv_text, 0, SIZE_SMS);
	memset(glb.sms_text, 0, SIZE_SMS);

	// by default AC and BATT are ok
	glb.status.curr |= SEND_SMS_AC_RESTORE;
	glb.status.curr |= SEND_SMS_BATT_RESTORE;

	// reset adcs
	glb.adc.batt.value = 0;
	glb.adc.batt.samples = 32;
	glb.adc.batt.skip_samples = 2;

	glb.adc.ac.value = 0;
	glb.adc.ac.samples = 32;
	glb.adc.ac.skip_samples = 2;

	glb.prev_sms_scan = millis();
	glb.prev_csq_test = millis();
	glb.tmr_batt_test = millis();
	glb.tmr_ac_test = millis();
	glb.cntr_ac = 0;
	glb.cntr_batt = 0;
}

void powerUpOrDown()
{
	pinMode(9, OUTPUT);
	digitalWrite(9, LOW);
	delay(1000);
	digitalWrite(9, HIGH);
	delay(2000);
	digitalWrite(9, LOW);
	delay(3000);
}


void loop()
{

	int cmd_len = 0;

	// Check GSM status
	//
	if (gsm.getStatus() != GSM::READY) {
		// Re-init modem
		DBG.println(F("Re-init GSM!"));
	}

	memset(glb.recv_text, 0, SIZE_SMS);
	memset(glb.sms_text, 0, SIZE_SMS);
	memset(glb.recv_num, 0, SIZE_TEL_NUM);

	// Check AC every 10 seconds
	if (millis() - glb.tmr_ac_test >= 10000) {
		glb.tmr_ac_test = millis();
		CheckAC();
	}

	// Check battery every 10 seconds
	if (millis() - glb.tmr_batt_test >= 10000) {
		glb.tmr_batt_test = millis();
		CheckBatt();
	}

	// Handle the relays
	RelaysHandler();

	// Check for serial commands
	if ((cmd_len = Serial.available()) > 0) {
		Serial.readBytesUntil('\n', glb.recv_text, SIZE_SMS);
		if (ParseSMS(NULL, glb.recv_text, glb.sms_text, UART_CMD)) {
			DBG.println(glb.sms_text);
		}
		Serial.flush();
	}

	// Check GSM CSQ
	//
	if (millis() - glb.prev_csq_test >= TMR_CSQ_TEST)
	{
		glb.prev_csq_test = millis();	//reset timer
		if (CheckGsmCSQ()) {
			if (glb.gsm.status == GSM_STATUS_NO_SIGNAL) {
				// reset modem
				DBG.println(F("No signal, reseting modem"));
				gsm.reset();
				gsm.begin(9600);
			}
		}
	}

	// Read incoming SMS if time has come
	//
	if (millis() - glb.prev_sms_scan >= TMR_SCAN_FOR_SMS) {
		glb.prev_sms_scan = millis();	//reset timer
		int sms_pos = sms.IsSMSPresent(SMS_UNREAD);

		if (sms_pos > 0) {
			DBG.print(F("New SMS received: "));	DBG.println(sms_pos);
			if (sms.GetSMS(sms_pos, glb.recv_num, glb.recv_text, SIZE_SMS)) {
				DBG.print(F("New SMS received from: ")); DBG.println(glb.recv_num);
				DBG.println(glb.recv_text);

				if (ParseSMS(glb.recv_num, glb.recv_text, glb.sms_text, GSM_CMD) > 0) {
					DBG.print(F("Sending SMS to: ")); DBG.println(glb.recv_num);
					glb.status.curr = SEND_SMS_RESP;
					glb.status.todo |= SEND_SMS_RESP;
				}
			}
			sms.DeleteSMS(sms_pos);
		}
		else if (sms_pos < 0) {
			// Modem needs re-init
			gsm.reset();
			gsm.begin(9600);
		}
		//digitalWrite(PIN_LED, ~digitalRead(PIN_LED));
	}
	

	// Check for TODO jobs
	//
	else if (glb.status.todo & SEND_SMS_BATT_FAULT) {
		glb.status.curr = SEND_SMS_BATT_FAULT;	
		PrepareDeviceStatusSMS(glb.sms_text, FSTR(strInd_028));	//"Battery is LOW!"
	}
	else if (glb.status.todo & SEND_SMS_AC_FAULT) {
		glb.status.curr = SEND_SMS_AC_FAULT;
		PrepareDeviceStatusSMS(glb.sms_text, FSTR(strInd_030));	//"AC power is LOST!"
	}
	else if (glb.status.todo & SEND_SMS_TEL_CHANGE) {
		//glb.status.curr = SEND_SMS_TEL_CHANGE;
	}
	else if (glb.status.todo & SEND_SMS_BATT_RESTORE) {
		glb.status.curr = SEND_SMS_BATT_RESTORE;
		PrepareDeviceStatusSMS(glb.sms_text, FSTR(strInd_029));	//"Battery RESTORED."
	}
	else if (glb.status.todo & SEND_SMS_AC_RESTORE) {
		glb.status.curr = SEND_SMS_AC_RESTORE;
		PrepareDeviceStatusSMS(glb.sms_text, FSTR(strInd_031));	//"AC power is RESTORED."
	}
	else if (glb.status.todo & SEND_SMS_RELAY_STATUS) {
		glb.status.curr = SEND_SMS_RELAY_STATUS;
		sprintf_P(glb.sms_text, FSTR(strInd_025),
			glb.relay[RELAY_INDEX_1].curr_status, (int)(glb.conf.relay[RELAY_INDEX_1].pulse_delay / 1000),
			glb.relay[RELAY_INDEX_2].curr_status, (int)(glb.conf.relay[RELAY_INDEX_2].pulse_delay / 1000),
			glb.relay[RELAY_INDEX_3].curr_status, (int)(glb.conf.relay[RELAY_INDEX_3].pulse_delay / 1000),
			glb.relay[RELAY_INDEX_4].curr_status, (int)(glb.conf.relay[RELAY_INDEX_4].pulse_delay / 1000));	//"RElay status: %d, delay: %d"
		
		//glb.status.todo &= ~SEND_SMS_RELAY_STATUS;
	}


	// Send appending SMSs
	//
	if (glb.gsm.status == GSM_STATUS_OK) {
		// try at least 4 times
		for (int i = 0; i < 4; i++) {
			if (glb.status.todo & glb.status.curr) {
				if (glb.conf.flags.enable_tel1 && CheckValidTelNum(glb.conf.tel1)) {
					if (sms.SendSMS(glb.conf.tel1, glb.sms_text)) {
					//if (0) {
						//sent OK
						DBG.println(F("SMS TEL1:"));
						DBG.println(glb.sms_text);
					}
					else {
						// failed
					}
				}
				if (glb.conf.flags.enable_tel2 && CheckValidTelNum(glb.conf.tel2)) {
					if (sms.SendSMS(glb.conf.tel2, glb.sms_text)) {
					//if (0) {
						// sent OK
						DBG.println(F("SMS TEL2:"));
						DBG.println(glb.sms_text);
					}
					else {
						// failed
					}
				}
				glb.status.todo &= ~glb.status.curr;	// remove from todo list
			}
		}
	}
}

void PrepareDeviceStatusSMS(char * resp, char * info)
{
	char tmp[30];
	uint16_t pre;

	sprintf(resp, "%s\r\n", const_device_text);

	if (info != NULL) {
		strcat_P(resp, info);	//"AC: OK"
		strcat(resp, "\r\n");
	}

	//ADC values
	strcat_P(resp, FSTR(strInd_017));	//"AC: OK"
	strcat(resp, "\r\n");
	strcat_P(resp, FSTR(strInd_019));	//"Batt: OK"
	strcat(resp, "\r\n");
	strcat_P(resp, FSTR(strInd_020));	//"Batt Volts: "
	pre = (uint16_t)glb.adc.batt.volts;
	sprintf(tmp, "%d.%d\r\n", pre, (uint16_t)(glb.adc.batt.volts*100.0) - (uint16_t)(pre * 100));
	strcat(resp, tmp);

	// Relays status
	sprintf_P(tmp, FSTR(strInd_027),	//"R1:%d,R2:%d,R3:%d,R4:%d\r\n" 
		glb.relay[RELAY_INDEX_1].curr_status, glb.relay[RELAY_INDEX_2].curr_status,
		glb.relay[RELAY_INDEX_3].curr_status, glb.relay[RELAY_INDEX_4].curr_status);
	strcat(resp, tmp);


	// Signal strength
	if (glb.gsm.rssi < 10)
		strcat_P(resp, FSTR(strInd_021));	//"GSM SIGNAL: LOW "
	else if (glb.gsm.rssi < 24)
		strcat_P(resp, FSTR(strInd_022));	//"GSM SIGNAL: MEDIUM "
	else if (glb.gsm.rssi < 99)
		strcat_P(resp, FSTR(strInd_023));	//"GSM SIGNAL: GOOD "
	snprintf(tmp, 10, "%d,%d\r\n", glb.gsm.rssi, glb.gsm.status);
	strcat(resp, tmp);
}


// Parse commands and return responses
//
int HandleCmd(char * data, const int len, char * resp, uint8_t is_master)
{
	int i;
	int resp_len = 0; // by default no string response

	TerminateGsmString(data, len);
	DBG.print(F("CMD: ")); DBG.println(data);

	if (!(strncmp_P(data, FSTR(strInd_005), 5))) {	//"IMEI?"
		sprintf_P(resp, FSTR(strInd_006));		//"IMEI: "
		strcat(resp, glb.conf.imei);
	}
	else if (!(strncmp_P(data, FSTR(strInd_001), 4))) {	//"TEL1"
		if (data[4] == '?') {
			sprintf_P(resp, FSTR(strInd_002));		//"TEL1: "
			strcat(resp, glb.conf.tel1);
		}
		else if (data[4] == '=') {
			if (CheckValidTelNum(&data[5])) {
				sprintf(glb.conf.tel1, "%s", &data[5]);
				cfg_conf->Save();
				sprintf_P(resp, FSTR(strInd_012));	//"New phone number is: "
				strcat(resp, glb.conf.tel1);
			}
			else {
				sprintf_P(resp, FSTR(strInd_013));	//"Phone number should start with +3069 with 13 digit length"
			}
		}
		DBG.println(resp);
	}
	else if (!(strncmp_P(data, FSTR(strInd_003), 4))) {	//"TEL2"
		if (data[4] == '?') {
			sprintf_P(resp, FSTR(strInd_004));	//"TEL2: "
			strcat(resp, glb.conf.tel2);
		}
		else if (data[4] == '=') {
			if (CheckValidTelNum(&data[5])) {
				sprintf(glb.conf.tel2, "%s", &data[5]);
				cfg_conf->Save();
				sprintf_P(resp, FSTR(strInd_012));	//"New phone number is: "
				strcat(resp, glb.conf.tel2);
			}
			else {
				sprintf_P(resp, FSTR(strInd_013));	//"Phone number should start with +3069 with 13 digit length"
			}
		}
		DBG.println(resp);
	}
	else if (!(strncmp_P(data, FSTR(strInd_007), 7))) {	//"PASSWD="
		if (!is_master) {
			sprintf_P(resp, FSTR(strInd_015));	//"You don't have the permittion to do this"
		}
		else {
			snprintf(glb.conf.passwd, SIZE_PASSW, "%s", &data[7]);
			sprintf_P(resp, FSTR(strInd_016));	//"New password set: "
			strcat(resp, glb.conf.passwd);
		}
		DBG.println(resp);
	}
	// Send device status 
	else if (!(strncmp_P(data, FSTR(strInd_008), 6))) {	//"STATUS"
		PrepareDeviceStatusSMS(resp, NULL);
	}
	// Send a test string to conf tel numbers
	else if (!(strncmp_P(data, FSTR(strInd_009), 7))) {	//"TESTSMS"
		if (CheckValidTelNum(glb.conf.tel1)) {
			sms.SendSMS(glb.conf.tel1, (char*)const_device_text);
			DBG.println(F("Sending to TEL1"));
		}
		if (CheckValidTelNum(glb.conf.tel2)) {
			sms.SendSMS(glb.conf.tel2, (char*)const_device_text);
			DBG.println(F("Sending to TEL2"));
		}
	}
	else if (!(strncmp_P(data, FSTR(strInd_024), 5))) {	//"RELAY"
		if (data[5] == '?') {
		}
		else if (data[5] == '=') {
			char * pch = strtok(&data[6], ",");

			// get relay index
			i = atoi(pch);
			DBG.print(F("index: ")); DBG.println(pch);

			// is relay index valid?
			if ((i >= 1) && (i <= NUM_OF_RELAYS)) {
				// get delay
				pch = strtok(NULL, ",");
				glb.conf.relay[i - 1].pulse_delay = (uint32_t)(1000 * (uint32_t)atoi(pch));
				DBG.print(F("value: ")); DBG.println(pch);

				// get initial value 
				pch = strtok(NULL, ",");
				glb.conf.relay[i - 1].init_value = atoi(pch);
				DBG.print(F("value: ")); DBG.println(pch);

				cfg_conf->Save();
			}
		}
		sprintf_P(resp, FSTR(strInd_025),
			glb.relay[RELAY_INDEX_1].curr_status, (uint16_t)(glb.conf.relay[RELAY_INDEX_1].pulse_delay / 1000),
			glb.relay[RELAY_INDEX_2].curr_status, (uint16_t)(glb.conf.relay[RELAY_INDEX_2].pulse_delay / 1000),
			glb.relay[RELAY_INDEX_3].curr_status, (uint16_t)(glb.conf.relay[RELAY_INDEX_3].pulse_delay / 1000),
			glb.relay[RELAY_INDEX_4].curr_status, (uint16_t)(glb.conf.relay[RELAY_INDEX_4].pulse_delay / 1000));	//"RElay status: %d, delay: %d"
	}
	else if (!(strncmp_P(data, FSTR(strInd_026), 5))) {	//"SET R"
		// get relay index (ASCII 49 = DEC 1
		if ((data[5] >= 49) && (data[5] <= 52)) {
			i = data[5] - 49;
			// get wanted relay value
			if (data[7] == 48) {
				glb.relay[i].wanted_status = RELAY_STATUS_OFF;
				//DBG.print(F("..SET R")); DBG.print(i+1); DBG.println(F(" TO 0"));
			}
			else if (data[7] == 49) {
				glb.relay[i].wanted_status = RELAY_STATUS_ON;
				//DBG.print(F("..SET R")); DBG.print(i+1); DBG.println(F(" TO 1"));
			}
		}
	}
	else if (!(strncmp_P(data, FSTR(strInd_010), 6))) {	//"FLAGS="

	}
	else if (!(strncmp_P(data, FSTR(strInd_011), 8))) {	//"DEFAULTS"
		cfg_conf->Defaults();
		DBG.println(F("Loaded default configuration in EEPROM"));
	}

	//ADC EEPROM
	else if (!(strncmp_P(data, FSTR(strInd_032), 6))) {	//"COEFAC"
		if (data[6] == '=') {
			char * ptr;
			glb.ee_adc.cal_coef_ac = strtod(&data[7], &ptr);
			cfg_adc->Save();
		}
		sprintf_P(resp, FSTR(strInd_032));		//"COEFAC: "
		dtostrf(glb.ee_adc.cal_coef_ac, 2, 3, &resp[7]);
		strcat(resp, "\r\n");
	}
	else if (!(strncmp_P(data, FSTR(strInd_033), 8))) {	//"COEFBATT"
		if (data[8] == '=') {
			char * ptr;
			glb.ee_adc.cal_coef_batt = strtod(&data[9], &ptr);
			cfg_adc->Save();
		}
		sprintf_P(resp, FSTR(strInd_033));		//"COEFAC: "
		dtostrf(glb.ee_adc.cal_coef_batt, 2, 3, &resp[9]);
		strcat(resp, "\r\n");
	}
	else if (!(strncmp_P(data, FSTR(strInd_034), 7))) {	//"THRESAC"
		if (data[7] == '=') {
			char * ptr;
			glb.ee_adc.thresshold_ac = strtod(&data[8], &ptr);
			cfg_adc->Save();
		}
		sprintf_P(resp, FSTR(strInd_034));		//"THRESAC: "
		dtostrf(glb.ee_adc.thresshold_ac, 2, 1, &resp[8]);
		strcat(resp, "\r\n");
	}
	else if (!(strncmp_P(data, FSTR(strInd_035), 9))) {	//"THRESBATT"
		if (data[9] == '=') {
			char * ptr;
			glb.ee_adc.thresshold_batt = strtod(&data[10], &ptr);
			cfg_adc->Save();
		}
		sprintf_P(resp, FSTR(strInd_035));		//"THRESBATT: "
		dtostrf(glb.ee_adc.thresshold_batt, 2, 1, &resp[10]);
		strcat(resp, "\r\n");
	}
	else if (!(strncmp_P(data, FSTR(strInd_038), 11))) {	//"ADCDEFAULTS"
		cfg_adc->Defaults();
		DBG.println(F("Loaded default configuration in EEPROM"));
	}
	else if (!(strncmp_P(data, FSTR(strInd_039), 5))) {	//"DEBUG"  
		if (data[5] == '=') {
			char * ptr;
			gsm.SetDebugLevel(atoi(&data[6]));
		}
		DBG.print(F("Debug level: "));	DBG.println(atoi(&data[6]));
	}


	DBG.print(F("SMS length: ")); DBG.println(strlen(resp));
	return(strlen(resp));
}



int ParseSMS(const char * sms_num, const char * sms_text, char * sms_resp, int serial_cmd)
{
	int resp = -1;	// by default not valid sms
	uint8_t is_master = 0;

	if (serial_cmd) {
		resp = HandleCmd((char*)sms_text, strlen(sms_text), sms_resp, 1);
	}
	else {
		boolean valid_user = false;

		DBG.print(F("RECV tel: "));
		DBG.println(sms_num);
		DBG.print(F("RECV text: "));
		DBG.println(sms_text);

		// Check if tel num is valid
		if (!strncmp(sms_num, glb.conf.tel1, 10)) {
			valid_user = true;
			is_master = 1;
		}
		else if (!strncmp(sms_num, glb.conf.tel2, 10)) {
			valid_user = true;
		}
		else {
			// check for passwd
			if (glb.conf.flags.enable_tel_passwd) {
				if (!strncmp(sms_text, glb.conf.passwd, 10)) {
					// delete previous tel numbers
					memset(glb.conf.tel1, SIZE_TEL_NUM, 0);
					memset(glb.conf.tel2, SIZE_TEL_NUM, 0);
					// save the new number
					strcpy(glb.conf.tel1, sms_num);
					cfg_conf->Save();
					// send SMS
					if (glb.conf.flags.on_passw_reset) {
						glb.status.todo |= SEND_SMS_PASSWD_RESET;
						sprintf_P(sms_resp, FSTR(strInd_037));	//"Deleted any previous TEL numbers.\r\nAdded phone number: "
						strcat(sms_resp, sms_num);
						resp = strlen(sms_resp);
						glb.status.todo |= SEND_SMS_RESP;
					}
				}
			}
		}

		if (valid_user == true) {
			resp = HandleCmd((char*)sms_text, strlen(sms_text), sms_resp, is_master);
			if (resp) {
				glb.status.todo |= SEND_SMS_RESP;
			}
		}
	}

	return(resp);
}


long readVcc() {
	long result;
	// Read 1.1V reference against AVcc
	ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
	delay(2); // Wait for Vref to settle
	ADCSRA |= _BV(ADSC); // Convert
	while (bit_is_set(ADCSRA, ADSC));
	result = ADCL;
	result |= ADCH << 8;
	result = 1125300L / result; // Back-calculate AVcc in mV
	return result;
}


/**
/ Check AC voltage and decide if it needs to send an SMS
*/
void CheckAC(void)
{
	uint8_t i;
	uint16_t adc_sum = 0;

	// skip samples
	for (i = 0; i < glb.adc.ac.skip_samples; i++) {
		analogRead(PIN_ADC_AC);
		delay(2);
	}
	// Read battery voltage
	for (i = 0; i < glb.adc.ac.samples; i++) {
		adc_sum += analogRead(PIN_ADC_AC);
		delay(2);
	}
	//DBG.print(F("SUM: ")); DBG.print(adc_sum); DBG.print(F(", samples: ")); DBG.println(i);
	// Get mean value
	glb.adc.ac.value = adc_sum / glb.adc.ac.samples;

	// Read reference voltage
	double vcc = readVcc() / 1000.0;
	//DBG.print(F("vcc: ")); DBG.println(vcc);

	// Calculate measured voltage
	glb.adc.ac.volts = (glb.adc.ac.value / 1023.0) * vcc * (double)((R1_ac + R2_ac) / (double)R2_ac) * glb.ee_adc.cal_coef_ac;

	DBG.print("AC: ADC: "); DBG.print(glb.adc.ac.value); DBG.print(F(", Volts: ")); DBG.println(glb.adc.ac.volts);

	// Low-pass filter
	if (glb.adc.ac.volts < glb.ee_adc.thresshold_ac) {
		if (glb.cntr_ac < CNTR_BATT_AC_FAULT) glb.cntr_ac++;
	}
	else {
		if (glb.cntr_ac) glb.cntr_ac--;
	}

	// Check for faults
	if (glb.cntr_ac == CNTR_BATT_AC_FAULT) {
		if (glb.status.curr & SEND_SMS_AC_RESTORE) {	// event detection
			glb.status.todo |= SEND_SMS_AC_FAULT;	// mark to send SMS	
			DBG.println("AC Lost");
		}
		glb.status.curr &= ~SEND_SMS_AC_RESTORE;
		glb.status.curr |= SEND_SMS_AC_FAULT;
	}
	else if (!glb.cntr_ac) {
		if (glb.status.curr & SEND_SMS_AC_FAULT) {		// event detection
			glb.status.todo |= SEND_SMS_AC_RESTORE;		// mark to send SMS
			DBG.println("AC Restore");
		}
		glb.status.curr &= ~SEND_SMS_AC_FAULT;
		glb.status.curr |= SEND_SMS_AC_RESTORE;
	}

}


/**
/ Check battery and decide is needs to send an SMS
*/
void CheckBatt(void)
{
	uint8_t i;
	uint16_t adc_sum = 0;
	
	// skip samples
	for (i = 0; i < glb.adc.batt.skip_samples; i++) {
		analogRead(PIN_ADC_BATT);
		delay(2);
	}
	// Read battery voltage
	for (i = 0; i < glb.adc.batt.samples; i++) {
		adc_sum += analogRead(PIN_ADC_BATT);
		delay(2);
	}
	//DBG.print(F("SUM: ")); DBG.print(adc_sum); DBG.print(F(", samples: ")); DBG.println(i);
	// Get mean value
	glb.adc.batt.value = adc_sum / glb.adc.batt.samples;

	// Read reference voltage
	double vcc = readVcc() / 1000.0;
	//DBG.print(F("vcc: ")); DBG.println(vcc);

	// Calculate measured voltage
	glb.adc.batt.volts = (glb.adc.batt.value / 1023.0) * vcc * (double)((R1_batt + R2_batt) / (double)R2_batt) * glb.ee_adc.cal_coef_batt;

	DBG.print("Battery: ADC: "); DBG.print(glb.adc.batt.value); DBG.print(F(", Volts: ")); DBG.println(glb.adc.batt.volts);

	// Low-pass filter
	if (glb.adc.batt.volts < glb.ee_adc.thresshold_batt) {
		if (glb.cntr_batt < CNTR_BATT_AC_FAULT) glb.cntr_batt++;
	}
	else {
		if (glb.cntr_batt) glb.cntr_batt--;
	}
	
	// Check for faults
	if (glb.cntr_batt == CNTR_BATT_AC_FAULT) {
		if (glb.status.curr & SEND_SMS_BATT_RESTORE) {	// event detection
			glb.status.todo |= SEND_SMS_BATT_FAULT;	// mark to send SMS	
			DBG.println("Battery LOW");
		}
		glb.status.curr &= ~SEND_SMS_BATT_RESTORE;
		glb.status.curr |= SEND_SMS_BATT_FAULT;
	}
	else if (!glb.cntr_batt) {
		if (glb.status.curr & SEND_SMS_BATT_FAULT) {		// event detection
			glb.status.todo |= SEND_SMS_BATT_RESTORE;		// mark to send SMS
			DBG.println("Battery Restore");
		}
		glb.status.curr &= ~SEND_SMS_BATT_FAULT;
		glb.status.curr |= SEND_SMS_BATT_RESTORE;
	}
	
}


// Handle relay outputs
void RelaysHandler(void)
{
	uint8_t i;

	for (i = 0; i < NUM_OF_RELAYS; i++) {
		if (glb.relay[i].wanted_status != glb.relay[i].curr_status) {
			glb.status.todo |= SEND_SMS_RELAY_STATUS;
			glb.relay[i].relay_tmr = 0;
			if (glb.relay[i].wanted_status == RELAY_STATUS_OFF) {
				digitalWrite(glb_relay_pin[i], LOW);
				DBG.print(F("Relay ")); DBG.print(i + 1); DBG.println(F(" OFF"));
			}
			else {
				digitalWrite(glb_relay_pin[i], HIGH);
				if (glb.conf.relay[i].pulse_delay && (glb.relay[i].relay_tmr == 0)) {
					glb.relay[i].relay_tmr = millis();
				}
				DBG.print(F("Relay ")); DBG.print(i + 1); DBG.println(F(" ON"));
			}
			glb.relay[i].curr_status = glb.relay[i].wanted_status;
		}

		if (glb.relay[i].relay_tmr && (millis() - glb.relay[i].relay_tmr >= glb.conf.relay[i].pulse_delay)) {
			glb.relay[i].relay_tmr = 0;	// stop timer
			digitalWrite(glb_relay_pin[i], LOW);
			glb.relay[i].curr_status = glb.relay[i].wanted_status = RELAY_STATUS_OFF;
			DBG.print(F("Relay ")); DBG.print(i + 1); DBG.println(F(" OFF"));
		}
	}

}
	

int CheckGsmCSQ(void)
{
	int result = 0;

	if (gsm.getCSQ(&glb.gsm.rssi, &glb.gsm.ber)) {
		if ((glb.gsm.rssi == 99)) 
			glb.gsm.status = GSM_STATUS_NO_SIGNAL;
		else 
			glb.gsm.status = GSM_STATUS_OK;
		result = 1;
	}
	return(result);
}


int CheckValidTelNum(char * num)
{
	return((!(strncmp_P(num, FSTR(strInd_014), 5)) && (strlen(num) == 13)));	//"+3069"
}


/**
* Check IMEI number and if it's different from EEPROM's then save the new one
*/
void CheckIMEI(void)
{
	char imei[50];
	
	if (gsm.getIMEI(imei)) {
		TerminateGsmString(imei, 50);
		// compare the IMEIs
		if (strncmp(imei, glb.conf.imei, SIZE_IMEI)) {
			memcpy(glb.conf.imei, imei, SIZE_IMEI);
			glb.conf.imei[SIZE_IMEI+1] = 0; // terminate string
			cfg_conf->Save();	// save config
			DBG.print(F("Saved new IMEI: "));
			DBG.println(glb.conf.imei);
		}
	}
	DBG.print(F("Got IMEI: ")); DBG.println(imei);
	
}


void TerminateGsmString(char * str, size_t len)
{
	int i;
	// remove \r\n
	for (i = 0; i < len; i++) {
		if ((str[i] == '\r') || (str[i] == '\n')) str[i] = 0;
	}
}
