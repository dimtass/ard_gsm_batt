#ifndef APP_STRINGS_H
#define APP_STRINGS_H

typedef enum {
	strInd_001 = 0,		//"TEL1"
	strInd_002,			//"TEL1: "
	strInd_003,			//"TEL2"
	strInd_004,			//"TEL2: "
	strInd_005,			//"IMEI?"
	strInd_006,			//"IMEI: "
	strInd_007,			//"PASSW="
	strInd_008,			//"STATUS"
	strInd_009,			//"TESTSMS"
	strInd_010,			//"FLAGS="
	strInd_011,			//"DEFAULTS"
	strInd_012,			//"New phone number is: "
	strInd_013,			//"Phone number should start with +3069 with 13 digit length"
	strInd_014,			//"+3069"
	strInd_015,			//"You don't have the permittion to do this"
	strInd_016,			//"New password is: "
	strInd_017,			//"AC: OK"
	strInd_018,			//"AC: FAIL"
	strInd_019,			//"Batt: OK"
	strInd_020,			//"Batt Volts: "
	strInd_021,			//"GSM Signal: LOW "
	strInd_022,			//"GSM Signal: MEDIUM "
	strInd_023,			//"GSM Signal: GOOD "
	strInd_024,			//"RELAY"
	strInd_025,			//"RELAY status"
	strInd_026,			//"SET R"
	strInd_027,         //"R1:%d,R2:%d,R3:%d,R4:%d\r\n"
	strInd_028,         //"Battery is LOW!"
	strInd_029,         //"Battery RESTORED."
	strInd_030,         //"AC power is LOST!"
	strInd_031,         //"AC power is RESTORED."
	strInd_032,         //"COEFAC: "
	strInd_033,         //"COEFBATT: "
	strInd_034,         //"THRESAC: "
	strInd_035,         //"THRESBATT: "
	strInd_036,         //"123456"
	strInd_037,         //"Deleted any previous TEL numbers. Added phone number: ";
	strInd_038,         //"ADCDEFAULTS";
	strInd_039,         //"DEBUG";
	strInd_040          //"";
} en_str_index;

const char strMSG_001[] PROGMEM = "TEL1";
const char strMSG_002[] PROGMEM = "TEL1: ";
const char strMSG_003[] PROGMEM = "TEL2";
const char strMSG_004[] PROGMEM = "TEL2: ";
const char strMSG_005[] PROGMEM = "IMEI?";
const char strMSG_006[] PROGMEM = "IMEI: ";
const char strMSG_007[] PROGMEM = "PASSWD=";
const char strMSG_008[] PROGMEM = "STATUS";
const char strMSG_009[] PROGMEM = "TESTSMS";
const char strMSG_010[] PROGMEM = "FLAGS=";
const char strMSG_011[] PROGMEM = "DEFAULTS";
const char strMSG_012[] PROGMEM = "New phone number is: ";
const char strMSG_013[] PROGMEM = "Phone number should start with +3069 with 13 digit length";
const char strMSG_014[] PROGMEM = "+3069";
const char strMSG_015[] PROGMEM = "You don't have the permittion to do this";
const char strMSG_016[] PROGMEM = "New password set: ";
const char strMSG_017[] PROGMEM = "AC: OK";
const char strMSG_018[] PROGMEM = "AC: FAIL";
const char strMSG_019[] PROGMEM = "Batt: OK";
const char strMSG_020[] PROGMEM = "Batt Volts: ";
const char strMSG_021[] PROGMEM = "GSM Signal: LOW ";
const char strMSG_022[] PROGMEM = "GSM Signal: MEDIUM ";
const char strMSG_023[] PROGMEM = "GSM Signal: HIGH ";
const char strMSG_024[] PROGMEM = "RELAY";
const char strMSG_025[] PROGMEM =   "R1 status: %d, delay: %d\r\n"
									"R2 status: %d, delay: %d\r\n"
									"R3 status: %d, delay: %d\r\n"
									"R4 status: %d, delay: %d\r\n";
const char strMSG_026[] PROGMEM = "SET R";
const char strMSG_027[] PROGMEM = "R1:%d,R2:%d,R3:%d,R4:%d\r\n";
const char strMSG_028[] PROGMEM = "Battery is LOW!";
const char strMSG_029[] PROGMEM = "Battery RESTORED.";
const char strMSG_030[] PROGMEM = "AC power is LOST!";
const char strMSG_031[] PROGMEM = "AC power is RESTORED.";
const char strMSG_032[] PROGMEM = "COEFAC: ";
const char strMSG_033[] PROGMEM = "COEFBATT: ";
const char strMSG_034[] PROGMEM = "THRESAC: ";
const char strMSG_035[] PROGMEM = "THRESBATT: ";
const char strMSG_036[] PROGMEM = "123456";
const char strMSG_037[] PROGMEM = "Deleted any previous TEL numbers.\r\nAdded phone number: ";
const char strMSG_038[] PROGMEM = "ADCDEFAULTS";
const char strMSG_039[] PROGMEM = "DEBUG";
const char strMSG_040[] PROGMEM = "";



const char* const string_table[] PROGMEM = { 
strMSG_001, strMSG_002, strMSG_003, strMSG_004, strMSG_005,
strMSG_006, strMSG_007, strMSG_008, strMSG_009, strMSG_010,
strMSG_011, strMSG_012, strMSG_013, strMSG_014, strMSG_015,
strMSG_016, strMSG_017, strMSG_018, strMSG_019, strMSG_020,
strMSG_021, strMSG_022, strMSG_023, strMSG_024, strMSG_025,
strMSG_026, strMSG_027, strMSG_028, strMSG_029, strMSG_030,
strMSG_031, strMSG_032, strMSG_033, strMSG_034, strMSG_035,
strMSG_036, strMSG_037, strMSG_038, strMSG_039, strMSG_040 
};

#define FSTR(X)  (char*)pgm_read_word(&(string_table[X]))

#endif